Virtual JBUG BPM Starter Kit slides from Feb 2015
-------------------------------------------------
[![Cover Slide](cover.png)](https://eschabell.gitlab.io/presentation-vjbug-bpm-starter-kit)


Released versions
-----------------
- v1.0 - session delivered on Feb 17, 2015 for online vJBug.
